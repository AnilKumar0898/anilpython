def myinput():
    a=int(input("Enter a number"))
    b=int(input("Enter another number"))
    c=int(input("Enter the last number"))
    return a,b,c

def myadd(a,b,c):
    return a+b+c

def myoutput(a,b,c,s):
    print("The sum of the numbers {}+{}+{}={}" .format(a,b,c,s))

def mymain():
    x,y,z=myinput()
    k=myadd(x,y,z)
    myoutput(x,y,z,k)
    
mymain()
